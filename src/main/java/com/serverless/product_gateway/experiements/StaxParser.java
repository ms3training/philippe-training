package com.serverless.product_gateway.experiements;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

public class StaxParser {

	private static final String COUNTER_KEY = "xPathCOUNT";
	private static String FILE_MODIFIER;

	public static void main(String argv[]) throws Exception {
		FILE_MODIFIER = "SMALL";
		parseFile("C:\\Users\\Phil\\Downloads\\xml\\pim_active_products_" + FILE_MODIFIER + ".xml");
		System.out.println("##########################################################################");
		FILE_MODIFIER = "LARGE";
		parseFile("C:\\Users\\Phil\\Downloads\\xml\\pim_active_products_" + FILE_MODIFIER + ".xml");
	}

	private static void parseFile(String filePath)
			throws FileNotFoundException, XMLStreamException, FactoryConfigurationError {
		System.out.println(filePath);
		START_TIME = System.currentTimeMillis();
		InputStream in = new FileInputStream(filePath);
		XMLStreamReader streamReader = XMLInputFactory.newInstance().createXMLStreamReader(in);
		parse(streamReader);
		// parseForTags(streamReader, "Products", "Product");
	}

	private static final ArrayDeque<String> XPATH = new ArrayDeque<>();
	private static final LinkedHashMap<String, LinkedHashMap<String, Integer>> TAGS_TO_ATTRIBUTES = new LinkedHashMap<>();
	private static int MAX_DEPTH = 0;
	private static long START_TIME = 0;
	private static long END_TIME = 0;

	public static void parseForTags(XMLStreamReader streamReader, String... tags) throws XMLStreamException {
		while (streamReader.hasNext()) {
			streamReader.next();
			if (streamReader.isStartElement()) {
				for (String tag : tags) {
					if (tag.equals(streamReader.getLocalName())) {
						parse(streamReader);
						break;
					}
				}
			} else if (streamReader.isEndElement()) {
				for (String tag : tags) {
					if (tag.equals(streamReader.getLocalName())) {
						if (XPATH.peek().equals(streamReader.getLocalName())) {
							XPATH.pop();
						} else {
							System.out.println("ERROR: PEEK MISMATCH");
						}
						break;
					}
				}
			}
		}
	}

	public static void parse(XMLStreamReader streamReader) throws XMLStreamException {
		while (streamReader.hasNext()) {
			streamReader.next();
			if (streamReader.isStartElement()) {
				String xPath = updateXPath(streamReader);
				HashMap<String, Integer> attributes = getAttributesAndUpdateAccessCounter(xPath);
				parseAttributes(streamReader, attributes);
			} else if (streamReader.isEndElement() && !XPATH.isEmpty()) {
				if (XPATH.peek().equals(streamReader.getLocalName())) {
					XPATH.pop();
				} else {
					System.out.println("ERROR: PEEK MISMATCH");
				}
			}
		}
		printMeta();
		clearMeta();
	}

	private static void clearMeta() {
		if (!XPATH.isEmpty()) {
			System.out.println("Error Left Over Tags: " + XPATH);
		}
		XPATH.clear();
		TAGS_TO_ATTRIBUTES.clear();
		MAX_DEPTH = 0;
		START_TIME = 0;
		END_TIME = 0;
		FILE_MODIFIER = "";
	}

	private static void printMeta() {
		END_TIME = System.currentTimeMillis();
		System.out.println();
		for (Entry<String, LinkedHashMap<String, Integer>> element : TAGS_TO_ATTRIBUTES.entrySet()) {
			System.out.print(element.getKey() + "," + element.getValue().toString().replaceAll("[{} ]+", ""));
			System.out.println();
		}
		System.out.println();
		System.out.println("# OF XPATHS = " + TAGS_TO_ATTRIBUTES.size());
		System.out.println();
		System.out.println("MAX DEPTH = " + MAX_DEPTH);
		System.out.println();
		System.out.println("EXEC TIME = " + (END_TIME - START_TIME) + " millis");
	}

	private static HashMap<String, Integer> getAttributesAndUpdateAccessCounter(String xPath) {
		LinkedHashMap<String, Integer> attributes;
		if (!TAGS_TO_ATTRIBUTES.containsKey(xPath)) {
			attributes = new LinkedHashMap<>();
			attributes.put(COUNTER_KEY, 1);
			TAGS_TO_ATTRIBUTES.put(xPath, attributes);
		} else {
			attributes = TAGS_TO_ATTRIBUTES.get(xPath);
			attributes.put(COUNTER_KEY, attributes.get(COUNTER_KEY) + 1);
		}
		return attributes;
	}

	private static String updateXPath(XMLStreamReader streamReader) {
		XPATH.push(streamReader.getLocalName());
		if (XPATH.size() > MAX_DEPTH) {
			MAX_DEPTH = XPATH.size();
		}
		String xPath = "";
		String endPoint = null;
		for (String tag : XPATH) {
			if (endPoint == null) {
				endPoint = tag;
			}
			xPath = "//" + tag + xPath;
		}
		return FILE_MODIFIER + "," + endPoint + "," + xPath;
	}

	public static void parseAttributes(XMLStreamReader streamReader, HashMap<String, Integer> attributes) {
		int attributeCount = streamReader.getAttributeCount();
		for (int i = 0; i < attributeCount; i++) {
			parseAttribute(streamReader, attributes, i);
		}
	}

	private static void parseAttribute(XMLStreamReader streamReader, HashMap<String, Integer> attributes, int i) {
		String attributeName = streamReader.getAttributeName(i).getLocalPart();
		Integer count = attributes.get(attributeName);
		if (count == null) {
			count = 1;
		} else {
			count++;
		}
		attributes.put(attributeName, count);
	}
}