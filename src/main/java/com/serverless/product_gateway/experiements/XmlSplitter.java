package com.serverless.product_gateway.experiements;

import java.io.File;
import java.io.FileReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;

//import com.ximpleware.AutoPilot;
//import com.ximpleware.VTDGen;
//import com.ximpleware.VTDNav;
//import com.ximpleware.XMLModifier;
import com.serverless.product_gateway.model.xml.Element;
import com.serverless.product_gateway.model.xml.ElementType;

public class XmlSplitter {
	// C:\Users\Phil\Desktop\test.xml
	// private static final String FILE_PATH = "C://Users//Phil//Desktop//";
	// C:\Users\Phil\Downloads\aws_training\project\xml
	private static final String FILE_PATH = "C://Users//Phil//Downloads//aws_training//project//xml//";

	// private static final String INPUT_FILE = "test.xml";
	private static final String INPUT_FILE = "pim_active_products_SMALL.xml";

	public static void main(String[] args) throws Exception {
		staxSplitter1();
	}

	public static void xSplitter() throws Exception {
		// place holder
	}

	// Looks good
	public static void staxSplitter1() throws Exception {
		XMLStreamReader xsr = XMLInputFactory.newInstance()
				.createXMLStreamReader(new FileReader(FILE_PATH + INPUT_FILE));
		Transformer t = TransformerFactory.newInstance().newTransformer();

		while (xsr.hasNext()) {
			xsr.next();
			System.out.print("Line: " + xsr.getLocation().getLineNumber());
			Element e = getElementIfSplitable(xsr);
			if (e == null) {
				continue;
			}
			System.out.println(" >>>> process: " + e);
			File file = new File(FILE_PATH + e.getLocalName() + "_" + INPUT_FILE);
			t.transform(new StAXSource(xsr), new StreamResult(file));
			System.out.println(file.getName());
		}
	}

	private static Element getElementIfSplitable(XMLStreamReader xsr) {
		if (!xsr.isStartElement()) {
			// can only split on start elements
			if (xsr.isCharacters()) {
				System.out.println(" >>>> skip: Text " + xsr.getText());
			} else if (xsr.isEndElement()) {
				System.out.println(" >>>> skip: End " + xsr.getLocalName());
			} else {
				System.out.println(" >>>> skip: non-start " + xsr.getEventType());
			}
			return null;
		}
		Element e = Element.get(xsr.getLocalName());
		if (e == null || !e.getElementType().equals(ElementType.SECTION)) {
			System.out.println(" >>>> skip: " + xsr.getLocalName());
			return null;
		}
		return e;
	}
}