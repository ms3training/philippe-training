package com.serverless.product_gateway.experiements;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.serverless.product_gateway.xml.ElementStreamer;
import com.serverless.product_gateway.xml.ElementStreamerUtil;
import com.twitter.common.objectsize.ObjectSizeCalculator;

public class ElementParser {

	private static final long MEM_LIMIT = 16777216;

	private static final int MAX_ITEM_COUNT = 10000;

	public static void main(String argv[]) throws Exception {
		// C:\Users\Phil\Downloads\aws_training\project\xml
		String fileLocation = "C:\\Users\\Phil\\Downloads\\aws_training\\project\\xml\\pim_active_products_";
		String fileSuffix = ".xml";
		String fileModifier = "SMALL";
		fileModifier = "LARGE";

		System.out.println("##########################################################################");
		parse(fileLocation + fileModifier + fileSuffix);
		System.out.println("##########################################################################");

		// parseFile(fileLocation + fileModifier + fileSuffix);
	}

	private static void parse(String filePath) throws FileNotFoundException, XMLStreamException, FactoryConfigurationError {
		System.out.println(filePath);
		InputStream in = new FileInputStream(filePath);
		long START_TIME = System.currentTimeMillis();
		parse(in);
		long END_TIME = System.currentTimeMillis();
		System.out.println("Time Millis: " + (END_TIME - START_TIME));
	}

	public static void parse(InputStream in) throws XMLStreamException, FactoryConfigurationError {
		ElementStreamer streamer = new ElementStreamer(in);
		int count = 0;
		long listSize = 0;
		long itemSize = 0;
		List<Item> allItems = new LinkedList<Item>();
		while (streamer.nextRoot()) {
			List<Item> items = ElementStreamerUtil.readRootElement(streamer);
			allItems.addAll(items);
			listSize += ObjectSizeCalculator.getObjectSize(items);
			for (Item item : items) {
				itemSize += ObjectSizeCalculator.getObjectSize(item);
			}
			count += items.size();
			if (listSize >= MEM_LIMIT || itemSize >= MEM_LIMIT || count >= MAX_ITEM_COUNT) {
				printSizes(count, listSize, itemSize);
				count = 0;
				itemSize = 0;
				listSize = 0;
			}
		}
		printSizes(count, listSize, itemSize);

		long allItemSize = 0;
		for (Item item : allItems) {
			allItemSize += ObjectSizeCalculator.getObjectSize(item);
		}
		printSizes(allItems.size(), ObjectSizeCalculator.getObjectSize(allItems), allItemSize);
	}

	private static void printSizes(int count, long listSize, long itemSize) {
		System.out.println("++++++++++++++++++++++++++++");
		if (count >= MAX_ITEM_COUNT) {
			System.out.println("Hit Max Items Count");
		} else {
			System.out.println("Hit Max Items Size");
		}
		System.out.println("Item Count: " + count);
		System.out.println("Item Size: " + itemSize);
		System.out.println("Item List Size: " + listSize);
		System.out.println("Avg Item Size: " + itemSize / count);
		System.out.println("Avg Item List Size: " + listSize / count);
		System.out.println("----------------------------");
	}
}
