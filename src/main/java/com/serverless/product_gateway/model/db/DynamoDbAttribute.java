package com.serverless.product_gateway.model.db;

public interface DynamoDbAttribute {

	public String getName();

	public String getExpressionVariable() ;

	public String getExpression();
}