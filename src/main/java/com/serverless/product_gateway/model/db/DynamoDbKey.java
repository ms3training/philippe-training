package com.serverless.product_gateway.model.db;

import java.util.HashMap;

public interface DynamoDbKey {
	
	/*
	 * Hash - partition key part of a dynamodb Key
	 * required for all key types
	 * required for query requests
	 */
	public DynamoDbAttribute getHash();

	/*
	 * Range - sort key part of a dynamodb Key
	 * required for local secondary indexes
	 * required for local secondary indexes query requests
	 */
	public DynamoDbAttribute getRange();
	
	/*
	 * Get key type
	 */
	public DynamoDbKeyType getType();
	
	/*
	 * Get key name
	 * 	If key is a primary key then this returns the table name
	 * 	Otherwise return the index name
	 */
	public String getName();
	
	/*
	 * returns the priority
	 */
	public int getPriority();
	
	/*
	 * returns true if this has priority over given key
	 */
	public boolean hasPriority(DynamoDbKey otherKey);
	
	/*
	 * validates key during compile time
	 */
	public static void validate(DynamoDbKey key) {
		assert key.getHash() != null;
		assert key.getType() != null;
		if (key.getType().equals(DynamoDbKeyType.SECONDARY_LOCAL_INDEX)) {
			assert key.getRange() != null;
		}
		assert key.getName() != null;
	}
	
	/*
	 * Returns key to start query operations, if null no available keys match and you will need to scan
	 */
	public static <E extends Enum<E> & DynamoDbKey> DynamoDbKey getQueryKey(Class<E> keyClass, HashMap<DynamoDbAttribute, Object> attributes) {
        if (attributes == null || attributes.isEmpty()) {
        	return null;
        }
		
		E[] keys = keyClass.getEnumConstants();
        E queryKey = null;
        for (E key: keys) {
        	if (!attributes.containsKey(key.getHash())) {
        		// Hash keys are required for running dynamodb queries
        		continue;
        	}
        	if (key.getType().equals(DynamoDbKeyType.SECONDARY_LOCAL_INDEX) && !attributes.containsKey(key.getRange())) {
        		// The range key is required if you want to use the secondary index
        		continue;
        	}
        	if (queryKey != null && queryKey.hasPriority(key)) {
        		// The current key has priority over this key even though they are both valid
        		continue;
        	}
        	queryKey = key;
        }
        return queryKey;
    }

	public static boolean containsAttribute(DynamoDbKey key, DynamoDbAttribute attribute) {
		return attribute.equals(key.getHash()) || attribute.equals(key.getRange());
	}
}