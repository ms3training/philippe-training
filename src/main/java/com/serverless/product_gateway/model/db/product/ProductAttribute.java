package com.serverless.product_gateway.model.db.product;

import com.serverless.product_gateway.model.db.DynamoDbAttribute;

public enum ProductAttribute implements DynamoDbAttribute {
	SKU("skuId"),
	STYLE_ID("styleId"),
	SUPER_STYLE_ID("superStyleId"),
	CATEGORY("category");

	private String name;
	private String expressionVariable;
	private String expression;

	ProductAttribute(String name) {
		this.name = name;
		this.expressionVariable = ":v_" + name;
		this.expression = name + " = " + expressionVariable;
	}

	public String getName() {
		return name;
	}

	public String getExpressionVariable() {
		return expressionVariable;
	}

	public String getExpression() {
		return expression;
	}
}