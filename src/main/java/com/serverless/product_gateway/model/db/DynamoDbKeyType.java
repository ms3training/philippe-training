package com.serverless.product_gateway.model.db;

public enum DynamoDbKeyType {
	PRIMARY_KEY(false),
	SECONDARY_LOCAL_INDEX(true),
	SECONDARY_GLOBAL_INDEX(true);
	
	private boolean isIndex;

	DynamoDbKeyType(boolean isIndex) {
		this.isIndex = isIndex;
	}
	
	public boolean isIndex() {
		return isIndex;
	}
}
