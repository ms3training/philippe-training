package com.serverless.product_gateway.model.db.product;

import com.serverless.product_gateway.model.db.DynamoDbAttribute;
import com.serverless.product_gateway.model.db.DynamoDbKey;
import com.serverless.product_gateway.model.db.DynamoDbKeyType;

public enum ProductKey implements DynamoDbKey {
	TABLE("product-table", DynamoDbKeyType.PRIMARY_KEY, ProductAttribute.SUPER_STYLE_ID, ProductAttribute.SKU, 2),
	SKU_INDEX("sku-index", DynamoDbKeyType.SECONDARY_GLOBAL_INDEX, ProductAttribute.SKU, null, 1),
	STYLE_INDEX("style-index", DynamoDbKeyType.SECONDARY_GLOBAL_INDEX, ProductAttribute.STYLE_ID, null, 1),
	CATEGORY_INDEX("category-index", DynamoDbKeyType.SECONDARY_GLOBAL_INDEX, ProductAttribute.CATEGORY, null, 3);

	private DynamoDbAttribute hash;
	private DynamoDbAttribute range;
	private DynamoDbKeyType type;
	private String name;
	private int priority;

	ProductKey(String name, DynamoDbKeyType type, DynamoDbAttribute hash, DynamoDbAttribute range, int priority) {
		this.hash = hash;
		this.range = range;
		this.type = type;
		this.name = name;
		this.priority = priority;
	}

	public DynamoDbAttribute getHash() {
		return hash;
	}

	public DynamoDbAttribute getRange() {
		return range;
	}

	public DynamoDbKeyType getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public int getPriority() {
		return priority;
	}

	public boolean hasPriority(DynamoDbKey key) {
		if (key instanceof ProductKey) {
			return this.priority < key.getPriority();
		}
		throw new UnsupportedOperationException();
	}
}