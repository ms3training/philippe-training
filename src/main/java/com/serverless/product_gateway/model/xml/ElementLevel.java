package com.serverless.product_gateway.model.xml;

public enum ElementLevel {
	ROOT,
	PARENT,
	CHILD,
	LEAF,
	DATA;
}
