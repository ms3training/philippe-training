package com.serverless.product_gateway.model.xml;

public enum ElementType {
	VALUE,
	LIST,
	OBJECT,
	SECTION;
}
