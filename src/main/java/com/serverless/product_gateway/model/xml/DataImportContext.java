package com.serverless.product_gateway.model.xml;

public class DataImportContext {

	private String sourceBucket;
	private String sourceKey;

	private String sanitizedBucket;
	private String sanitizedKey;

	private String destinationTable;

	public String getSourceBucket() {
		return sourceBucket;
	}

	public void setSourceBucket(String sourceBucket) {
		this.sourceBucket = sourceBucket;
	}

	public String getSourceKey() {
		return sourceKey;
	}

	public void setSourceKey(String sourceKey) {
		this.sourceKey = sourceKey;
	}

	public String getSanitizedBucket() {
		return sanitizedBucket;
	}

	public void setSanitizedBucket(String sanitizedBucket) {
		this.sanitizedBucket = sanitizedBucket;
	}

	public String getSanitizedKey() {
		return sanitizedKey;
	}

	public void setSanitizedKey(String sanitizedKey) {
		this.sanitizedKey = sanitizedKey;
	}

	public String getDestinationTable() {
		return destinationTable;
	}

	public void setDestinationTable(String destinationTable) {
		this.destinationTable = destinationTable;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataImportContext [sourceBucket=");
		builder.append(sourceBucket);
		builder.append(", sourceKey=");
		builder.append(sourceKey);
		builder.append(", sanitizedBucket=");
		builder.append(sanitizedBucket);
		builder.append(", sanitizedKey=");
		builder.append(sanitizedKey);
		builder.append(", destinationTable=");
		builder.append(destinationTable);
		builder.append("]");
		return builder.toString();
	}
}