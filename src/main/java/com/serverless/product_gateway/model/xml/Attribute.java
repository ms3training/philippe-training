package com.serverless.product_gateway.model.xml;

public enum Attribute {
	ATTRIBUTE_ID("AttributeID", AttributeUsage.NAME),
	USER_TYPE_ID("UserTypeID", AttributeUsage.TYPE),
	ID("ID", AttributeUsage.FIELD),
	CLASSIFICATION_ID("ClassificationID", AttributeUsage.FIELD),
	PARENT_ID("ParentID", AttributeUsage.FIELD),
	UNIT_ID("UnitID", AttributeUsage.FIELD),
	TYPE("Type", AttributeUsage.FIELD);

	private String localName;
	private AttributeUsage[] usages;

	Attribute(String localName, AttributeUsage... usages) {
		this.localName = localName;
		this.usages = usages;
	}
	
	public String getLocalName() {
		return localName;
	}
	
	public AttributeUsage[] getUsages() {
		return usages;
	}

	public boolean isType() {
		for (AttributeUsage usage : usages) {
			if (usage.equals(AttributeUsage.TYPE)) {
				return true;
			}
		}
		return false;
	}
	
	public static Attribute get(String localName) {
		for (Attribute element: Attribute.values()) {
			if (element.localName.equals(localName)) {
				return element;
			}
		}
		return null;
	}
}