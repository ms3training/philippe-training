package com.serverless.product_gateway.model.xml;

import java.util.Map;

public enum ElementContext {
	SUPER_STYLE(
			Element.PRODUCT,
			ElementLevel.ROOT,
			null,
			XmlConstants.SUPER_STYLE_TYPE,
			XmlConstants.SUPER_STYLE_TRANSFORMER),
	STYLE(
			Element.PRODUCT,
			ElementLevel.PARENT,
			ElementContext.SUPER_STYLE,
			XmlConstants.STYLE_TYPE,
			XmlConstants.STYLE_TRANSFORMER),
	SKU(
			Element.PRODUCT,
			ElementLevel.DATA,
			ElementContext.STYLE,
			XmlConstants.SKU_TYPE,
			XmlConstants.SKU_TRANSFORMER);

	private Element element;
	private ElementLevel level;
	private String type;
	private Map<String, String> fieldTransformer;

	private ElementContext parent;

	ElementContext(Element element, ElementLevel level, ElementContext parent, String type,
			Map<String, String> fieldTransformer) {
		this.element = element;
		this.level = level;
		this.type = type;
		this.parent = parent;
		this.fieldTransformer = fieldTransformer;
	}
	
	public Element getElement() {
		return element;
	}
	
	public ElementLevel getLevel() {
		return level;
	}
	
	public ElementContext getParentContext() {
		return parent;
	}
	
	public boolean isRoot() {
		return ElementLevel.ROOT.equals(level);
	}
	
	public boolean isData() {
		return ElementLevel.DATA.equals(level);
	}

	public String getFieldName(String key) {
		return fieldTransformer.get(key);
	}

	public static ElementContext getElementObjectContext(Element element, String type) {
		for (ElementContext context : ElementContext.values()) {
			if (context.element.equals(element) && context.type.equals(type)) {
				return context;
			}
		}
		return null;
	}
}
