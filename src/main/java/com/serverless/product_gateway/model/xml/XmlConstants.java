package com.serverless.product_gateway.model.xml;

import java.util.HashMap;
import java.util.Map;

public class XmlConstants {

	public static final Map<Element, String> ELEMENT_TO_TABLE = new HashMap<>();
	static {
		ELEMENT_TO_TABLE.put(Element.PRODUCTS, "product-table");
	}
	
	public static final String getTable(String localName) {
		Element key = Element.get(localName);
		return getTableName(key);
	}

	public static final String getTableName(Element key) {
		return ELEMENT_TO_TABLE.get(key);
	}

	protected static final String SUPER_STYLE_TYPE = "Superstyle";
	protected static final String STYLE_TYPE = "Style";
	protected static final String SKU_TYPE = "SKU";

	// id
	// name
	// displayName
	// productname
	// category
	protected static final Map<String, String> SUPER_STYLE_TRANSFORMER = new HashMap<>();
	static {
		SUPER_STYLE_TRANSFORMER.put("Product-ID", "superStyleId");
		SUPER_STYLE_TRANSFORMER.put("Name", "superStyleName");
		SUPER_STYLE_TRANSFORMER.put("displayname", "displayName");
		SUPER_STYLE_TRANSFORMER.put("Product-ParentID", "categoryTree");
		SUPER_STYLE_TRANSFORMER.put("productname", "productName");
	}

	// id
	// name
	// productName
	// displayName
	// category
	// site
	// description
	// brand
	// mainImage
	// additionalImages
	// colors
	protected static final Map<String, String> STYLE_TRANSFORMER = new HashMap<>();
	static {
		STYLE_TRANSFORMER.put("Product-ID", "styleId");
		STYLE_TRANSFORMER.put("Name", "styleName");
		STYLE_TRANSFORMER.put("productname", "productName");
		STYLE_TRANSFORMER.put("ClassificationReference-ClassificationID", "category");
	}

	// id
	// highestRetailPrice
	// currentRetailPrice
	// msrpprice
	// onsale
	protected static final Map<String, String> SKU_TRANSFORMER = new HashMap<>();
	static {
		SKU_TRANSFORMER.put("Product-ID", "skuId");
		SKU_TRANSFORMER.put("Name", "skuName");
	}
}