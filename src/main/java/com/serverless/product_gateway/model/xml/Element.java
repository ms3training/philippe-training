package com.serverless.product_gateway.model.xml;

public enum Element {
	PRODUCTS("Products", ElementType.SECTION),
	PRODUCT("Product", ElementType.OBJECT),
	NAME("Name", ElementType.VALUE),
	CLASSIFICATION_REFERENCE("ClassificationReference", ElementType.VALUE),
	VALUE("Value", ElementType.VALUE),
	MULTI_VALUE("MultiValue", ElementType.LIST);

	private String localName;
	private ElementType elementType;

	Element(String localName, ElementType elementType) {
		this.localName = localName;
		this.elementType = elementType;
	}
	
	public String getLocalName() {
		return localName;
	}
	
	public ElementType getElementType() {
		return elementType;
	}

	public static Element get(String localName) {
		for (Element e : Element.values()) {
			if (e.localName.equals(localName)) {
				return e;
			}
		}
		return null;
	}
}