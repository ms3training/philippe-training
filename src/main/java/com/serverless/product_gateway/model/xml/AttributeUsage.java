package com.serverless.product_gateway.model.xml;

public enum AttributeUsage {
	FIELD,
	NAME,
	TYPE;
}
