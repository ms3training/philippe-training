package com.serverless.product_gateway.function;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.stepfunctions.AWSStepFunctions;
import com.amazonaws.services.stepfunctions.AWSStepFunctionsClientBuilder;
import com.amazonaws.services.stepfunctions.model.StartExecutionRequest;
import com.amazonaws.services.stepfunctions.model.StartExecutionResult;
import com.amazonaws.util.json.Jackson;
import com.serverless.product_gateway.model.xml.DataImportContext;

/**
 * The sole purpose of this function is to trigger the AWS Step Function to
 * process the newly uploaded xml.
 */
public class XmlDataImportInvoker implements RequestHandler<S3Event, Object> {

	// DynamoDB table name for storing article metadata.
	private static final String STATE_MACHINE_ARN = System.getenv("STATE_MACHINE_ARN");

	@Override
	public Object handleRequest(S3Event event, Context context) {
		System.out.println("S3Event: " + event.toJson());

		System.out.println("StateMachine ARN: " + STATE_MACHINE_ARN);

		System.out.println("Get Step Function Client");
		AWSStepFunctions sf = AWSStepFunctionsClientBuilder.standard().build();
		for (S3EventNotificationRecord record : event.getRecords()) {
			System.out.println("Build Execution Request");
			StartExecutionRequest executionRequest = new StartExecutionRequest();
			// Set ARN (required)
			executionRequest.setStateMachineArn(STATE_MACHINE_ARN);

			// Set input (required even if empty {})
			DataImportContext dataImportContext = new DataImportContext();
			dataImportContext.setSourceBucket(record.getS3().getBucket().getName());
			dataImportContext.setSourceKey(record.getS3().getObject().getKey());
			executionRequest.setInput(Jackson.toJsonString(dataImportContext));

			// Set name (optional)
			executionRequest.setName("XML_DATA_IMPORT_OF_" + record.getS3().getObject().getKey());

			// Use async client for non blocking execution (fire and forget)
			System.out.println("Run Execution Request");
			StartExecutionResult result = sf.startExecution(executionRequest);
			System.out.println("Results: " + result.getExecutionArn());
		}
		return null;
	}
}