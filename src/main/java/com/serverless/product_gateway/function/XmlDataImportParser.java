package com.serverless.product_gateway.function;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLStreamException;

import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.Tag;
import com.serverless.product_gateway.db.DynamoUtils;
import com.serverless.product_gateway.model.xml.DataImportContext;
import com.serverless.product_gateway.s3.S3Utils;
import com.serverless.product_gateway.xml.ElementStreamer;
import com.serverless.product_gateway.xml.ElementStreamerUtil;

public class XmlDataImportParser implements RequestHandler<DataImportContext, Object> {

	private static final int MAX_BATCH_ITEMS = 25;

	private List<Item> batchItems = new LinkedList<Item>();

	@Override
	public Object handleRequest(DataImportContext dataImportContext, Context context) {
		System.out.println("Max Batch Write Items: " + MAX_BATCH_ITEMS);

		System.out.println("DataImportContext: " + dataImportContext);

		System.out.println("Get S3 client");
		final AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();
		System.out.println("Get XML");
		S3Object s3Object = S3Utils.get(s3, dataImportContext.getSanitizedBucket(), dataImportContext.getSanitizedKey());
		System.out.println("Import XML");
		List<Tag> resultTags = importXmlIntoDb(s3Object, dataImportContext.getDestinationTable());
		System.out.println("Update Source tags with results");
		S3Utils.setTags(s3, dataImportContext.getSourceBucket(), dataImportContext.getSourceKey(), resultTags);
		System.out.println("Remove imported XML");
		S3Utils.delete(s3, dataImportContext.getSanitizedBucket(), dataImportContext.getSanitizedKey());
		return null;
	}

	public List<Tag> importXmlIntoDb(S3Object s3Object, String table) {
		int count = 0;
		try {
			final DynamoDB dynamoDb = DynamoUtils.getDynamoDB();
			ElementStreamer streamer = new ElementStreamer(s3Object.getObjectContent());
			while (streamer.nextRoot()) {
				List<Item> items = ElementStreamerUtil.readRootElement(streamer);
				batchWriteQueue(dynamoDb, table, items);
				count += items.size();
			}
			batchWrite(dynamoDb, table, batchItems);
			return buildResultTag(table, count);
		} catch (XMLStreamException e) {
			System.err.println(e);
			throw new IllegalStateException("Stream failed for " + s3Object.getKey(), e);
		}
	}

	private List<Tag> buildResultTag(String table, int count) {
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(table + " Import", "Imported " + count + " items"));
		return tags;
	}

	public void batchWriteQueue(DynamoDB dynamoDb, String table, List<Item> items) {
		if (items == null || items.isEmpty()) {
			return;
		}

		for (Item item : items) {
			if (batchItems.size() == MAX_BATCH_ITEMS) {
				System.out.println("Will BatchWrite " + batchItems.size() + " items");
				batchWrite(dynamoDb, table, batchItems);
				System.out.println("Start new batch");
				batchItems = new LinkedList<>();
			}
			batchItems.add(item);
		}
	}

	public void batchWrite(DynamoDB dynamoDb, String table, List<Item> items) {
		if (items == null || items.isEmpty()) {
			return;
		}

		TableWriteItems writeItems = new TableWriteItems(table).withItemsToPut(items);
		BatchWriteItemOutcome outcome = dynamoDb.batchWriteItem(writeItems);
		do {
			// Check for unprocessed keys which could happen if you exceed provisioned throughput
			Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();

			if (outcome.getUnprocessedItems().size() > 0) {
				System.out.println("Retrieving unprocessed items " + outcome.getUnprocessedItems().size() + "/" + items.size());
				outcome = dynamoDb.batchWriteItemUnprocessed(unprocessedItems);
			}
		} while (outcome.getUnprocessedItems().size() > 0);
	}
}