package com.serverless.product_gateway.function;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stax.StAXSource;
import javax.xml.transform.stream.StreamResult;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.serverless.product_gateway.model.xml.DataImportContext;
import com.serverless.product_gateway.model.xml.Element;
import com.serverless.product_gateway.model.xml.ElementType;
import com.serverless.product_gateway.model.xml.XmlConstants;
import com.serverless.product_gateway.s3.S3Utils;

public class XmlDataImportSplitter implements RequestHandler<DataImportContext, Map<String, DataImportContext>> {
	
	// Lambda functions can only write to the tmp dir, all other attempts will result in errors
	private static final String FILE_PATH = "/tmp/";

	private static final String SANITIZED_BUCKET_NAME = System.getenv("SANITIZED_BUCKET_NAME");

	@Override
	public Map<String, DataImportContext> handleRequest(DataImportContext dataImportContext, Context context) {
		dataImportContext.setSanitizedBucket(SANITIZED_BUCKET_NAME);
		System.out.println("Source Bucket: " + dataImportContext.getSourceBucket());
		System.out.println("Source Key: " + dataImportContext.getSourceKey());
		System.out.println("Sanitized Bucket: " + dataImportContext.getSanitizedBucket());

		System.out.println("Get S3 Client");
		final AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();

		System.out.println("Get XML File");
		S3Object s3Object = S3Utils.get(s3, dataImportContext.getSourceBucket(), dataImportContext.getSourceKey());

		System.out.println("Split XML");
		Map<String, DataImportContext> result = splitXml(s3, s3Object, dataImportContext);

		System.out.println("Split Result: " + result);
		return result;
	}

	private Map<String, DataImportContext> splitXml(AmazonS3 s3, S3Object s3Object, DataImportContext dataImportContext) {
		Map<String, DataImportContext> result = new HashMap<String, DataImportContext>();
		try {
			XMLStreamReader xsr = XMLInputFactory.newInstance().createXMLStreamReader(s3Object.getObjectContent());
			Transformer t = TransformerFactory.newInstance().newTransformer();
			while (xsr.hasNext()) {
				xsr.next();
				Element splitableElement = getElementIfSplitable(xsr);
				if (splitableElement == null) {
					continue;
				}
				System.out.println("Line: " + xsr.getLocation().getLineNumber() + " >>> process section: " + splitableElement);
				// Create new file for xml section
				File file = new File(FILE_PATH + splitableElement.getLocalName() + "_" + s3Object.getKey());

				// Copy source section into new file
				t.transform(new StAXSource(xsr), new StreamResult(file));

				// Save new file to S3 bucket
				S3Utils.put(s3, dataImportContext.getSanitizedBucket(), file.getName(), file);

				// Build import context for new file
				DataImportContext diContext = buildDataImportContext(dataImportContext, file, splitableElement);
				result.put(diContext.getDestinationTable(), diContext);
				System.out.println(" >>> section processed: " + splitableElement);
			}
		} catch (Exception e) {
			System.err.println(e);
			// Rollback
			for (Entry<String, DataImportContext> entry : result.entrySet()) {
				S3Utils.delete(s3, entry.getValue().getSanitizedBucket(), entry.getValue().getSanitizedKey());
			}
			throw new IllegalStateException("Failed to split source file", e);
		}
		return result;
	}

	private DataImportContext buildDataImportContext(DataImportContext dataImportContext, File file, Element element) {
		DataImportContext diContext = new DataImportContext();
		diContext.setSourceBucket(dataImportContext.getSourceBucket());
		diContext.setSourceKey(dataImportContext.getSourceKey());
		diContext.setSanitizedBucket(dataImportContext.getSanitizedBucket());
		diContext.setSanitizedKey(file.getName());
		diContext.setDestinationTable(XmlConstants.getTableName(element));
		return diContext;
	}

	private Element getElementIfSplitable(XMLStreamReader xsr) {
		if (!xsr.isStartElement()) {
			// can only split on start elements
			return null;
		}
		Element e = Element.get(xsr.getLocalName());
		if (e == null || !e.getElementType().equals(ElementType.SECTION)) {
			return null;
		}
		return e;
	}
}