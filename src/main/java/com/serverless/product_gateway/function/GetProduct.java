package com.serverless.product_gateway.function;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.serverless.product_gateway.db.DynamoUtils;
import com.serverless.product_gateway.db.QueryExecutor;
import com.serverless.product_gateway.model.api.ApiRequest;
import com.serverless.product_gateway.model.api.ApiResponse;
import com.serverless.product_gateway.model.db.product.ProductAttribute;
import com.serverless.product_gateway.model.db.product.ProductKey;

/**
 * Lambda function that triggered by the API Gateway event "GET /product". It
 * reads query parameter "id" for the article id and retrieves the content of
 * that article from the underlying S3 bucket and returns the content as the
 * payload of the HTTP Response.
 */
public class GetProduct implements RequestHandler<ApiRequest, ApiResponse> {
	// DynamoDB table name for storing article metadata.
	private static final String TABLE_NAME = System.getenv("TABLE_NAME");

	@Override
	public ApiResponse handleRequest(ApiRequest request, Context context) {
		System.out.println("Table name: " + TABLE_NAME);
		
		System.out.println("Build query executor for table");
		QueryExecutor<ProductKey, ProductAttribute> executer = new QueryExecutor<>(
				DynamoUtils.getTable(TABLE_NAME), ProductKey.class, ProductAttribute.class);

		ApiResponse response = new ApiResponse();
		try {
			System.out.println("Run query given params: " + request.getQueryStringParameters());
			ArrayList<Map<String, Object>> items = executer.execute(request.getQueryStringParameters());
			response.setStatusCode(200);
			response.setBody(items.toString());
		} catch (Exception e) {
			System.out.println(e);
			response.setStatusCode(500);
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			response.setBody(sw.toString());
		}
		System.out.println("ReturnCode: " + response.getStatusCode());
		System.out.println("ReturnBody: " + response.getBody());
		return response;
	}
}