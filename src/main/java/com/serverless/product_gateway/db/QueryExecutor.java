package com.serverless.product_gateway.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.internal.IteratorSupport;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.spec.ScanSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.serverless.product_gateway.model.db.DynamoDbAttribute;
import com.serverless.product_gateway.model.db.DynamoDbKey;

public class QueryExecutor<K extends Enum<K> & DynamoDbKey, A extends Enum<A> & DynamoDbAttribute> {

	private Table table;
	private Class<K> keyClass;
	private Class<A> attributeClass;

	public QueryExecutor(Table table, Class<K> keys, Class<A> attributes) {
		this.table = table;
		this.keyClass = keys;
		this.attributeClass = attributes;
	}

	public ArrayList<Map<String, Object>> execute(Map<String, String> map) {
		HashMap<DynamoDbAttribute, Object> attributes = buildAttributeMap(map);
		DynamoDbKey key = DynamoDbKey.getQueryKey(keyClass, attributes);
		if (key != null) {
			return executeQuery(key, attributes);
		} else {
			return executeScan(attributes);
		}
	}

	private ArrayList<Map<String, Object>> executeScan(HashMap<DynamoDbAttribute, Object> attributes) {
		ScanSpec spec = buildScanSpec(attributes);
		ItemCollection<ScanOutcome> result = table.scan(spec);
		IteratorSupport<Item, ScanOutcome> resultIterator = result.iterator();

		ArrayList<Map<String, Object>> items = new ArrayList<>();
		while (resultIterator.hasNext()) {
			items.add(resultIterator.next().asMap());
		}
		return items;
	}

	private ArrayList<Map<String, Object>> executeQuery(DynamoDbKey key, HashMap<DynamoDbAttribute, Object> attributes) {
		QuerySpec spec = buildQuerySpec(key, attributes);
		ItemCollection<QueryOutcome> result = null;
		if (key.getType().isIndex()) {
			result = table.getIndex(key.getName()).query(spec);
		}
		result = table.query(spec);
		IteratorSupport<Item, QueryOutcome> resultIterator = result.iterator();

		ArrayList<Map<String, Object>> items = new ArrayList<>();
		while (resultIterator.hasNext()) {
			items.add(resultIterator.next().asMap());
		}
		return items;

	}

	private ScanSpec buildScanSpec(HashMap<DynamoDbAttribute, Object> attributes) {
		if (attributes == null || attributes.isEmpty()) {
			return new ScanSpec();
		}

		StringBuilder filterExpression = new StringBuilder();
		ValueMap valueMap = new ValueMap();
		for (Entry<DynamoDbAttribute, Object> attributeValuePair : attributes.entrySet()) {
			DynamoDbAttribute attribute = attributeValuePair.getKey();
			filterExpression.append(attribute.getExpression());
			valueMap.with(attribute.getExpressionVariable(), attributeValuePair.getValue());
		}
		ScanSpec spec = new ScanSpec();
		if (filterExpression.length() != 0 && !valueMap.isEmpty()) {
			spec.withFilterExpression(filterExpression.toString());
			spec.withValueMap(valueMap);
		}

		return spec;
	}

	private QuerySpec buildQuerySpec(DynamoDbKey key, HashMap<DynamoDbAttribute, Object> attributes) {
		StringBuilder keyConditionExpression = new StringBuilder();
		StringBuilder filterExpression = new StringBuilder();
		ValueMap valueMap = new ValueMap();
		for (Entry<DynamoDbAttribute, Object> attributeValuePair : attributes.entrySet()) {
			DynamoDbAttribute attribute = attributeValuePair.getKey();
			if (DynamoDbKey.containsAttribute(key, attribute)) {
				keyConditionExpression.append(attribute.getExpression());
			} else {
				filterExpression.append(attribute.getExpression());
			}
			valueMap.with(attribute.getExpressionVariable(), attributeValuePair.getValue());
		}
		QuerySpec spec = new QuerySpec();
		spec.withKeyConditionExpression(keyConditionExpression.toString());
		if (filterExpression.length() != 0) {
			spec.withFilterExpression(filterExpression.toString());
		}
		spec.withValueMap(valueMap);
		return spec;
	}

	private HashMap<DynamoDbAttribute, Object> buildAttributeMap(Map<String, String> inputAttributes) {
		if (inputAttributes == null || inputAttributes.isEmpty()) {
			return null;
		}
		HashMap<DynamoDbAttribute, Object> attributes = new HashMap<>();
		for (A attribute : attributeClass.getEnumConstants()) {
			if (!inputAttributes.containsKey(attribute.getName())) {
				continue;
			}
			attributes.put(attribute, inputAttributes.get(attribute.getName()));
		}
		return attributes;
	}
}
