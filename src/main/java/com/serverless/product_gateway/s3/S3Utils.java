package com.serverless.product_gateway.s3;

import java.io.File;
import java.util.List;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectTagging;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.SetObjectTaggingRequest;
import com.amazonaws.services.s3.model.Tag;

public class S3Utils {

	public static S3Object get(AmazonS3 s3, S3EventNotificationRecord record) {
		String bucket = record.getS3().getBucket().getName();
		String key = record.getS3().getObject().getKey();
		return get(s3, bucket, key);
	}

	public static S3Object get(AmazonS3 s3, String bucket, String key) {
		return s3.getObject(new GetObjectRequest(bucket, key));
	}

	public static PutObjectResult put(AmazonS3 s3, S3Object s3Object, File file) {
		return put(s3, s3Object.getBucketName(), s3Object.getKey(), file);
	}

	public static PutObjectResult put(AmazonS3 s3, String bucket, String key, File file) {
		return s3.putObject(new PutObjectRequest(bucket, key, file));
	}

	public static void delete(AmazonS3 s3, S3Object s3Object) {
		delete(s3, s3Object.getBucketName(), s3Object.getKey());
	}

	public static void delete(AmazonS3 s3, String bucket, String key) {
		s3.deleteObject(new DeleteObjectRequest(bucket, key));
	}

	public static void setTags(AmazonS3 s3, S3Object s3Object, List<Tag> tags) {
		setTags(s3, s3Object.getBucketName(), s3Object.getKey(), tags);
	}

	public static void setTags(AmazonS3 s3, String bucket, String key, List<Tag> tags) {
		s3.setObjectTagging(new SetObjectTaggingRequest(bucket, key, new ObjectTagging(tags)));
	}
}