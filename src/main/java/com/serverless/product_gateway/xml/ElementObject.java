package com.serverless.product_gateway.xml;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.serverless.product_gateway.model.xml.ElementContext;

public class ElementObject {

	private ElementContext context;
	private Map<String, Object> fields;
	private ElementObject parent;
	
	public ElementObject(ElementContext context) {
		this(null, context);
	}

	public ElementObject(ElementObject parent, ElementContext context) {
		this.parent = parent;
		this.context = context;
		this.fields = new HashMap<>();
	}

	public void putField(String key, Object value) {
		String name = context.getFieldName(key);
		if (name == null) {
			return;
		}
		fields.put(name, value);
	}

	public Item toItem() {
		Item item;
		if (parent != null) {
			item = parent.toItem();
		} else {
			item = new Item();
		}
		for (Entry<String, Object> field : fields.entrySet()) {
			item.with(field.getKey(), field.getValue());
		}
		return item;
	}

	public ElementContext getContext() {
		return context;
	}
}
