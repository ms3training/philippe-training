package com.serverless.product_gateway.xml;

import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.stream.XMLStreamException;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.serverless.product_gateway.model.xml.ElementType;

public class ElementStreamerUtil {

	public static List<Item> readRootElement(ElementStreamer streamer) throws XMLStreamException {
		if (streamer.getContext() == null || !streamer.getContext().isRoot()) {
			return null;
		}
		ElementObject root = readElementObject(streamer, null);
		if (root == null) {
			return null;
		}

		List<ElementObject> data = new LinkedList<>();
		parseElement(streamer, root, data);

		List<Item> items = new LinkedList<>();
		for (ElementObject eo : data) {
			items.add(eo.toItem());
		}
		return items;
	}

	private static void parseElement(ElementStreamer streamer, ElementObject eo, List<ElementObject> data) throws XMLStreamException {
		while (streamer.hasNext()) {
			if (!streamer.next()) {
				continue;
			}
			if (streamer.isStartElement()) {
				switch (streamer.getElement().getElementType()) {
					case LIST:
						readElementList(streamer, eo);
						break;
					case OBJECT:
						ElementObject nextEo = readElementObject(streamer, eo);
						if (nextEo != null) {
							parseElement(streamer, nextEo, data);
						}
						break;
					case VALUE:
						readElementValue(streamer, eo);
						break;
					default:
						System.out.println("Unsupported Element type: " + streamer.getElement().getElementType());
						break;
				}
			} else if (streamer.isEndElement() && eo.getContext().getElement().equals(streamer.getElement())) {
				break;
			}
		}
		if (eo.getContext().isData()) {
			data.add(eo);
		}
	}

	/*
	 * validates and builds element object for the current element
	 */
	public static ElementObject readElementObject(ElementStreamer streamer, ElementObject parent) {
		if (!ElementType.OBJECT.equals(streamer.getElement().getElementType()) || streamer.getContext() == null) {
			return null; // TODO ADD ERROR MSG
		}

		if (streamer.getContext().getParentContext() != null
				&& (parent == null || !streamer.getContext().getParentContext().equals(parent.getContext()))) {
			return null;
		}
		ElementObject eo = new ElementObject(parent, streamer.getContext());

		readAttributeFields(streamer, eo);
		return eo;
	}

	/*
	 * currently only supports simple string values
	 */
	public static void readElementValue(ElementStreamer streamer, ElementObject eo) throws XMLStreamException {
		if (!streamer.isStartElement() || !streamer.getElement().getElementType().equals(ElementType.VALUE)) {
			return;
		}
		readAttributeFields(streamer, eo);

		eo.putField(streamer.getName(), streamer.getValue());
	}

	/*
	 * currently only supports simple string lists
	 */
	public static void readElementList(ElementStreamer streamer, ElementObject eo) throws XMLStreamException {
		if (!streamer.isStartElement() || !streamer.getElement().getElementType().equals(ElementType.LIST)) {
			return;
		}
		readAttributeFields(streamer, eo);

		String listName = streamer.getName();
		List<String> list = new LinkedList<>();

		while (streamer.hasNext()) {
			streamer.next();
			if (streamer.isStartElement() && streamer.getElement().getElementType().equals(ElementType.VALUE)) {
				list.add(streamer.getValue());
			} else if (streamer.isEndElement() && streamer.getElement().getElementType().equals(ElementType.LIST)) {
				break;
			}
		}

		eo.putField(listName, list);
	}

	public static void readAttributeFields(ElementStreamer streamer, ElementObject eo) {
		for (Entry<String, String> attribute : streamer.getFieldAttributes().entrySet()) {
			eo.putField(attribute.getKey(), attribute.getValue());
		}
	}
}