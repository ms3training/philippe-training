package com.serverless.product_gateway.xml;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.serverless.product_gateway.model.xml.Attribute;
import com.serverless.product_gateway.model.xml.AttributeUsage;
import com.serverless.product_gateway.model.xml.Element;
import com.serverless.product_gateway.model.xml.ElementContext;
import com.serverless.product_gateway.model.xml.ElementType;

public class ElementStreamer {

	private final ArrayDeque<String> XPATH = new ArrayDeque<>();

	private final XMLStreamReader streamReader;

	public ElementStreamer(XMLStreamReader streamReader) {
		this.streamReader = streamReader;
	}

	public ElementStreamer(InputStream in) throws XMLStreamException, FactoryConfigurationError {
		this(XMLInputFactory.newInstance().createXMLStreamReader(in));
	}

	public ElementStreamer(String filePath)
			throws FileNotFoundException, XMLStreamException, FactoryConfigurationError {
		this(new FileInputStream(filePath));
	}

	// CURRENT ELEMENT DATA FIELDS
	// Holds the current element
	private Element element = null;

	// Only used when element is of type object
	private ElementContext context = null;

	// Only used when element is of type value
	private String name = null;

	// Holds onto field type attributes for later processing
	private HashMap<String, String> fieldAttributes = null;

	// Getters
	public Element getElement() {
		return element;
	}

	public ElementContext getContext() {
		return context;
	}

	public String getName() {
		return name;
	}

	public String getValue() throws XMLStreamException {
		String value = streamReader.getElementText();
		updateXPath();
		return value;
	}

	public HashMap<String, String> getFieldAttributes() {
		return fieldAttributes;
	}

	public boolean hasNext() throws XMLStreamException {
		return streamReader.hasNext();
	}

	/*
	 * Returns true if current element matches a desired element, as in has data
	 * of interest
	 */
	public boolean next() throws XMLStreamException {
		clearMeta();
		streamReader.next();
		updateXPath();
		return loadMeta();
	}

	/*
	 * Returns true when a root start element is found, false if there it
	 * reached the end of file without finding one
	 */
	public boolean nextRoot() throws XMLStreamException {
		while (hasNext()) {
			if (!next()) {
				// Not a point of interest
				continue;
			}
			if (!isStartElement()) {
				// Not a starting point
				continue;
			}
			if (getContext() == null || !getContext().isRoot()) {
				// Not a root element
				continue;
			}
			return true;
		}
		return false;
	}

	public boolean isStartElement() {
		return streamReader.isStartElement();
	}

	public boolean isEndElement() {
		return streamReader.isEndElement();
	}

	private void updateXPath() {
		if (streamReader.isStartElement()) {
			XPATH.push(streamReader.getLocalName());
		} else if (streamReader.isEndElement()) {
			if (XPATH.peek().equals(streamReader.getLocalName())) {
				XPATH.pop();
			} else {
				String errorMsg = "INVALID END TAG: EXPECTED "
						+ XPATH.peek()
						+ " BUT WAS "
						+ streamReader.getLocalName()
						+ " @ LINE NUMBER "
						+ streamReader.getLocation().getLineNumber();
				throw new UnsupportedOperationException(errorMsg);
			}
		}
	}

	private void clearMeta() {
		element = null;
		context = null;
		name = null;
		fieldAttributes = null;
	}

	private boolean loadMeta() throws XMLStreamException {
		if (!isStartElement() && !isEndElement()) {
			return false;
		}

		element = Element.get(streamReader.getLocalName());
		if (element == null || element.getElementType().equals(ElementType.SECTION)) {
			return false;
		}

		if (!isStartElement()) {
			return true;
		}
		name = streamReader.getLocalName();

		// Load attribute meta
		String contextType = null;
		int attributeCount = streamReader.getAttributeCount();
		ArrayList<Integer> fieldAttributeIndexs = new ArrayList<>(attributeCount);
		for (int i = 0; i < attributeCount; i++) {
			Attribute attribute = Attribute.get(streamReader.getAttributeLocalName(i));
			if (attribute == null) {
				continue;
			}
			for (AttributeUsage usage : attribute.getUsages()) {
				switch (usage) {
					case FIELD:
						fieldAttributeIndexs.add(i);
						break;
					case NAME:
						name = streamReader.getAttributeValue(i);
						break;
					case TYPE:
						contextType = streamReader.getAttributeValue(i);
						break;
					default:
						System.out.println("Unsupported Attribute Usage: " + usage);
						break;
				}
			}
		}

		fieldAttributes = new HashMap<>();
		for (int i : fieldAttributeIndexs) {
			fieldAttributes.put(name + "-" + streamReader.getAttributeName(i), streamReader.getAttributeValue(i));
		}

		if (ElementType.OBJECT.equals(element.getElementType())) {
			context = ElementContext.getElementObjectContext(element, contextType);
		}
		return true;
	}
}
